import { Link } from "react-router-dom";

const Navbar = ({user}) => {
    const logout = () => {
        window.open("https://dc18.coe.psu.ac.th/api/auth/logout", "_self");
      };
    return (<div className="navbar">
        <span className="logo">
           <Link className ="link" to ="/" >star wars</Link>
            </span>{
             user ? (
                <ul className="list">
                  <li className="listItem">
                    <img
                      src={user.photos[0].value}
                      alt=""
                      className="avatar"
                    />
                  </li>
                  <li className="listItem">{user.displayName}</li>
                  <li className="listItem" onClick={logout}>
                    Logout
                  </li>
                </ul>
              
        ) : (<Link className="link" to="login">Login</Link>)
    }
    </div>
    );
} ;
export default Navbar;