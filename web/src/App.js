import './App.css';
import Navbar from './Navbar';
import Home from './Home';
import Login from "./Login";
//import axios from 'axios';
import Post from './Post';
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import { useEffect, useState } from "react";

function App() {
  const [user, setUser] = useState(null);

  useEffect(() => {
    const getUser = () => {
      fetch("https://dc18.coe.psu.ac.th/api/auth/login/success", {
        method: "GET",
        credentials: "include",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "Access-Control-Allow-Credentials": true,
        },
      })
        .then((response) => {
          if (response.status === 200) return response.json();
          throw new Error("authentication has been failed!");
        })
        .then((resObject) => {
          setUser(resObject.user);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    getUser();
  }, []);

   
  return (
    <BrowserRouter>
    <div>
      <Navbar user = {user}/>
      <Routes>
        <Route path="/" element={<Home />}/>
        <Route path="/login" element={user ? <Navigate to ="/" /> : <Login/>}/>
        <Route path="/post/:id" element={user ? <Post /> : <Navigate to ="/login"/>}/>
      </Routes>
    </div>
    </BrowserRouter>
  );
}

export default App;
