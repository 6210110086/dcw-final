import { useLocation } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";


const Post = () => {
    const location = useLocation();
    const id = location.pathname.split("/")[2];
    const [post, setpost] = useState(null);

    useEffect(() => {
        const reqA = async () => {
          let result = await axios.get(`https://dc18.coe.psu.ac.th/api/${id}`);
          console.log(result.data)
          setpost(result.data);
        };
        reqA();
      }, []);

      return post ? (
        <div className="post">
          <img src={post.img} alt="" className="postImg" />
          <h1 className="postTitle">{post.title}</h1>
          <p className="postDesc">{post.desc}</p>
          <p className="postLongDesc">{post.longDesc}</p>
        </div>
      ):(<div></div>);
    };
    
export default Post;